<?php
require "bootstrap.php";
use Chatter\Models\Product;

$app = new \Slim\App();
$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});
$app->get('/customers/{number}', function($request, $response,$args){
   return $response->write('Your customer number is '.$args['number']);
});
$app->get('/customers/{cnumber}/products/{pnumber}', function($request, $response,$args){
   return $response->write('Your customer number is '.$args['cnumber'].', and your product is '.$args['pnumber']);
});
/*$app->get('/users', function($request, $response,$args){
    $_user = new User();
    $users = $_user->all();

    $payload = [];
    foreach($users as $u){
        $payload[$u->id] = [
            "name" => $u->name,
            "phonenumber" => $u->phonenumber,

        ];
    }
    return $response->withStatus(200)->withJson($payload);
});
*/

$app->get('/products', function($request, $response,$args){
    $_product = new Product();
    $Products = $_product->all();

    $payload = [];
    foreach($Products as $p){
        $payload[$p->id] = [
            "name" => $p->name,
            "price" => $p->price,

        ];
    }
    return $response->withStatus(200)->withJson($payload);
});
/*
$app->post('/users', function($request, $response,$args){
    $name = $request->getParsedBodyParam('name','');
    $phonenumber = $request->getParsedBodyParam('phonenumber','');
    $_user = new User();
    $_user->name = $name;
    $_user->phonenumber = $phonenumber;

    $_user->save();
       
    if($_user->id){
        $payload = ['user_id' => $_user->id];
        return $response->withStatus(201)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});
*/
$app->put('/products/{product_id}', function($request, $response, $args){
    $name = $request->getParsedBodyParam('name','');
    $price = $request->getParsedBodyParam('price','');
    $_product = Product::find($args['product_id']);
    $_product->name = $name;
    $_product->price = $price;   

   
    if($_product->save()){
        $payload = ['product_id' => $_product->id,"result" => "The product has been updates successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});


$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});
$app->run();
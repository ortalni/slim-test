<?php 

namespace Chatter\Middleware;

class Logging {
    public function __invoke($request,$response,$next){
        //before rout
        //כותב את הסטרינג לתוך קובץ
        error_log($request->getMethod()."--". $request->getUri()."\r\n"
        ,3,"log.txt");
        $response = $next ($request,$response);
        //after route
        return $response;

    }

}